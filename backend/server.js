const express = require('express')

const app = express()

app.use(express.json())

const route = require('./routes/movies')

app.use('/movie', route)


app.listen(4000, '0.0.0.0', () => {
    console.log('backend started on port 4000')
})