const express = require('express')

const router = express.Router()

const db = require('../db')
const utils = require('../utils')

router.get('/', (request, response) => {
    
    response.send('Checking....')
})

router.get('/:name', (request, response) => {
    const {name} = request.params
    const statement = ` SELECT * FROM movies WHERE movie_title = '${name}'`

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.post('/', (request, response) => {
    const {name, date, time, director} = request.body

    const statement = ` INSERT INTO 
    movies(movie_title, movie_release_date,movie_time,director_name) 
    VALUES('${name}', '${date}', '${time}', '${director}') `

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.put('/:id', (request, response) => {
    const {id} = request.params
    const {date, time} = request.body

    const statement = ` UPDATE movies 
    SET movie_release_date = '${date}',
    movie_time = '${time}'
    WHERE movie_id = '${id}'`

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.delete('/:id', (request, response) => {
    const {id} = request.params

    const statement = ` DELETE FROM movies 
    WHERE movie_id = '${id}'`

    db.execute(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

module.exports = router